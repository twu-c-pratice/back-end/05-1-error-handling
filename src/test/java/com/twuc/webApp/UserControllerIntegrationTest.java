package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
@AutoConfigureMockMvc
class UserControllerIntegrationTest {
    @Autowired TestRestTemplate testRestTemplate;
    @Autowired MockMvc mockMvc;

    @Test
    void should_get_404_when_have_exception() {
        ResponseEntity<Void> entity =
                testRestTemplate.getForEntity("/api/errors/default", Void.class);
        assertEquals(404, entity.getStatusCode().value());
    }

    @Test
    void should_get_500_exception() throws Exception {
        mockMvc.perform(get("/api/errors/default")).andExpect(status().is(500));
    }

    @Test
    void should_handle_illegal_argument_exception() {
        ResponseEntity<ExceptionMessage> entity =
                testRestTemplate.getForEntity(
                        "/api/errors/illegal-argument", ExceptionMessage.class);
        assertEquals(500, entity.getStatusCode().value());
        assertEquals("Something wrong with the argument", entity.getBody().getExceptionContent());
    }

    @Test
    void should_handle_no_pointer_exception() {
        ResponseEntity<ExceptionMessage> entity =
                testRestTemplate.getForEntity("/api/errors/null-pointer", ExceptionMessage.class);
        assertEquals(418, entity.getStatusCode().value());
        assertEquals("Something wrong with the argument", entity.getBody().getExceptionContent());
    }

    @Test
    void should_handle_arithmetic_exception() {
        ResponseEntity<ExceptionMessage> entity =
                testRestTemplate.getForEntity("/api/errors/arithmetic", ExceptionMessage.class);
        assertEquals(418, entity.getStatusCode().value());
        assertEquals("Something wrong with the argument", entity.getBody().getExceptionContent());
    }
}
