package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
class SisterControllerTest {
    @Autowired TestRestTemplate testRestTemplate;

    @Test
    void should_get_exception() {
        ResponseEntity<ExceptionMessage> entity =
                testRestTemplate.getForEntity(
                        "/api/sister-errors/illegal-argument", ExceptionMessage.class);
        assertEquals(418, entity.getStatusCode().value());
        assertEquals(MediaType.APPLICATION_JSON_UTF8, entity.getHeaders().getContentType());
        assertEquals(
                "Something wrong with brother or sister.", entity.getBody().getExceptionContent());
    }
}
