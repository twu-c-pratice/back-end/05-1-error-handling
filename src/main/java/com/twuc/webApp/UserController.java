package com.twuc.webApp;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Null;
import java.security.AccessControlException;

@RestController
public class UserController {

    @GetMapping("/api/errors/default")
    public void getRuntimeException() {
        throw new RuntimeException();
    }

    @GetMapping("/api/errors/illegal-argument")
    void getIllegalArgumentException() {
        throw new IllegalArgumentException("Something wrong with the argument");
    }

    @GetMapping("/api/exceptions/access-control-exception")
    void getAccessControlException() {
        throw new AccessControlException("Something wrong with the access");
    }

    @ExceptionHandler(RuntimeException.class)
    ResponseEntity runtimeExceptionHandler(RuntimeException exception) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new ExceptionMessage(exception.getMessage()));
    }

    @ExceptionHandler(IllegalArgumentException.class)
    ResponseEntity illegalArgumentException(IllegalArgumentException exception) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(new ExceptionMessage(exception.getMessage()));
    }

    @GetMapping("/api/errors/null-pointer")
    void getNullPointerException() {
        throw new NullPointerException("Something wrong with the argument");
    }

    @GetMapping("/api/errors/arithmetic")
    void getArithmeticException() {
        throw new ArithmeticException("Something wrong with the argument");
    }

    @ExceptionHandler(value = {NullPointerException.class, ArithmeticException.class})
    ResponseEntity argumentException(IllegalArgumentException exception) {
        return ResponseEntity.status(418).body(new ExceptionMessage(exception.getMessage()));
    }
}
