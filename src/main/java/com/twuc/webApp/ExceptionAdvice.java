package com.twuc.webApp;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(IllegalArgumentException.class)
    ResponseEntity illegalArgumentException(IllegalArgumentException exception) {
        return ResponseEntity.status(418)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(new ExceptionMessage("Something wrong with brother or sister."));
    }
}
