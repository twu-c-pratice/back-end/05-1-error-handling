package com.twuc.webApp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SisterController {
    @GetMapping("/api/sister-errors/illegal-argument")
    void getException() {
        throw new IllegalArgumentException();
    }
}
