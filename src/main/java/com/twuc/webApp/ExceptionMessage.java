package com.twuc.webApp;

public class ExceptionMessage {
    private String exceptionContent;

    public ExceptionMessage() {}

    public ExceptionMessage(String exceptionContent) {

        this.exceptionContent = exceptionContent;
    }

    public String getExceptionContent() {
        return exceptionContent;
    }
}
